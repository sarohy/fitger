package com.example.awais.acclometer.CoreCode;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.MainActivity;

import java.util.Calendar;

import static java.security.AccessController.getContext;

/**
 * Created by Awais on 11/22/2016.
 */

public class TimeService extends Service {

   public int hours;
    public int minutes;
   SharedPreferences sharedPreferences;
    boolean SecondTime=false;

    @Override
    public void onCreate() {
        Toast.makeText(this,"oncreate",Toast.LENGTH_SHORT).show();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        hours=sharedPreferences.getInt("R_hours",-1);
        minutes=sharedPreferences.getInt("R_minutes",-1);


        runAsForeground();
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent,int flags,int startId) {

      Toast.makeText(this,"TimeService Started",Toast.LENGTH_SHORT).show();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

       if(intent.getBooleanExtra("fromSC",false)) {
           hours = intent.getIntExtra("hours", -1);
           minutes = intent.getIntExtra("minutes", -1);
       }

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("R_hours", hours);
        editor.putInt("R_minutes",minutes);
        editor.apply();
        editor.commit();

        if(hours!=-1&&minutes!=-1) {
            startCheckingTime();
        }
        else
            Toast.makeText(this,"didnt get Time",Toast.LENGTH_SHORT).show();

      return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    public void startCheckingTime(){
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Calendar c = Calendar.getInstance();

                int Hr24=c.get(Calendar.HOUR_OF_DAY);

                int Min=c.get(Calendar.MINUTE);
                if(Min==minutes && Hr24==hours ){
                   // Intent intent =new Intent(getApplicationContext(),MainActivity.class);
                  // intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    //intent.putExtra("timeService",true);
                    //startActivity(intent);

                    Intent in=new Intent();
                    in.setAction("b");
                    sendBroadcast(in);
                    minutes=-2;
                }



                handler.postDelayed(this, 1000);
            }

        });

    }
    public void fun(){

        while (true){

        }

    }
    private void runAsForeground(){
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent= PendingIntent.getActivity(this, 0,notificationIntent, 0);

        Notification notification=new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.sceduale)
                .setContentTitle("Fitger Reminder")
                .setTicker("Time Updated")
                .setContentText("We will start your running at you selected time")
                .setContentIntent(pendingIntent).build();

        startForeground(1332, notification);

    }
    @Override
    public void onDestroy() {
        Toast.makeText(this,"on Destroy Called",Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}

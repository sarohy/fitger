package com.example.awais.acclometer.CoreCode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Awais on 12/4/2016.
 */


public class BroadCastForReminder extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intent2=new Intent(context,TimeService.class);
        context.stopService(intent2);


        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("IsStart", true);
        editor.apply();
        editor.commit();
        Intent intent1=new Intent(context,CalculationService.class);
        intent1.putExtra("weight",sharedpreferences.getInt("weight",60));
        intent1.putExtra("height",sharedpreferences.getFloat("height",5f));
        intent1.putExtra("fromM",true);
        context.startService(intent1);
    }
}

package com.example.awais.acclometer.CoreCode;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.awais.acclometer.R;
import com.special.ResideMenu.ResideMenu;

public class webview extends Fragment {

    WebView myWebview;
    ProgressBar pb;
    int ProgrusStatus=4;
    View v;
    Button srch;
    EditText e;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_webview,container,false);

        // getWindow().requestFeature(Window.FEATURE_PROGRESS);
       // getActivity().requestWindowFeature(Window.FEATURE_PROGRESS);

        myWebview=(WebView)v.findViewById(R.id.mywebview);
        pb=(ProgressBar)v.findViewById(R.id.myprogressBar);
        srch=(Button)v.findViewById(R.id.googlesearch) ;
        e=(EditText)v.findViewById(R.id.google_ed);
        pb.setVisibility(View.INVISIBLE);

        myWebview.getSettings().setJavaScriptEnabled(true);

        myWebview.getSettings().setJavaScriptEnabled(true);

        final Activity activity = getActivity();
        myWebview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                pb.setProgress(ProgrusStatus);
                ProgrusStatus=ProgrusStatus+8;
                //activity.setProgress(progress * 1000);

            }


        });
        myWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Error! " + description, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                ProgrusStatus=0;
                pb.setVisibility(View.INVISIBLE);

            }
        });
        String sHtmlTemplate = "<html><head></head><body><img src=\"file:///android_asset/logo4.jpg\"></body></html>";

        myWebview.loadDataWithBaseURL(null, sHtmlTemplate, "text/html", "utf-8",null);

        srch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String st=new String();
                st=e.getText().toString();

                pb.setVisibility(View.VISIBLE);
                pb.setProgress(4);
                myWebview.loadUrl("http://www.google.com.pk/search?site=&source=hp&q="+st);


            }
        });


        return v;
    }

    public void searchClick(View view) {
        pb.setVisibility(View.VISIBLE);
        pb.setProgress(4);
        myWebview.loadUrl("http://developer.android.com/");
    }


}

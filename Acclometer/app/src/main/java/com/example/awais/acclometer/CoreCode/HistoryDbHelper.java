package com.example.awais.acclometer.CoreCode;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Awais on 11/19/2016.
 */

public class HistoryDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "history.db";

    public HistoryDbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
         String sql="create table history (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                 " steps INTEGER," +
                 "distance FLOAT," +
                 "calories FLOAT," +
                 "date TEXT,"+
                 "time TEXT," +
                 "type TEXT)";
        String sql2="create table history2 (_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," +
                " steps INTEGER," +
                "distance FLOAT," +
                "calories FLOAT," +
                "date TEXT,"+
                "time TEXT)";

        db.execSQL(sql);
        db.execSQL(sql2);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS history");
       db.execSQL("DROP TABLE IF EXISTS history2");
              onCreate(db);
    }
}

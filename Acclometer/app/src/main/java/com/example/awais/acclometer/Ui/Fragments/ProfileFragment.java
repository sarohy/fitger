package com.example.awais.acclometer.Ui.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.MainActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ProfileFragment extends Fragment {


    private SharedPreferences sharedpreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.profile, container, false);
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        weightText= (EditText) v.findViewById(R.id.input_weight);
        heightText= (EditText) v.findViewById(R.id.input_height);
        nameText= (EditText) v.findViewById(R.id.input_name);
        save= (Button) v.findViewById(R.id.btn_save);
        int s=sharedpreferences.getInt("weight",60);
        float f=sharedpreferences.getFloat("height",5f);
        weightText.setText(String.valueOf(s));
        heightText.setText(String.valueOf(f));
        nameText.setText(sharedpreferences.getString("name",""));
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt("weight", (int) Float.parseFloat(weightText.getText().toString()));
                editor.putFloat("height", Float.parseFloat(heightText.getText().toString()));
                editor.putString("name", nameText.getText().toString());
                editor.apply();
                editor.commit();
                Toast.makeText(getActivity(),"Saved",Toast.LENGTH_SHORT).show();
            }
        });
        return v;
    }


    EditText heightText;
    EditText weightText;
    EditText nameText;
    Button save;
}

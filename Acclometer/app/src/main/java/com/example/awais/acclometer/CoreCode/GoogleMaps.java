package com.example.awais.acclometer.CoreCode;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Context;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.awais.acclometer.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

public class GoogleMaps extends Fragment implements OnMapReadyCallback {
    int i = 0;
    boolean trigger=false;
    private Button btn_zoom;
    private GoogleMap mMap;
    private TextView text;
    private LatLng lastLocation;
    private Marker marker;
    Criteria criteria = new Criteria();
    private View v;
    private LocationManager locationManager;
    LocationListener locListener = new LocationListener() {
        @Override
        public void onStatusChanged(String provider, int status,
                                    Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onLocationChanged(Location location) {
            //location.setAccuracy(1000);
            lastLocation = marker.getPosition();
            marker.remove();
            LatLng current = new LatLng(location.getLatitude(), location.getLongitude());
            marker = mMap.addMarker(new MarkerOptions().position(current).title("Current Location"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(current));

            drawPrimaryLinePath();
        }


    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_google_maps,container,false);
        MapFragment mapFragment = (MapFragment)getActivity().getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        //locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(LOCATION_SERVICE);
        if ( !locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }

        if((LocationManager) getActivity().getApplicationContext().getSystemService(LOCATION_SERVICE)==null){
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setMessage("Check the GPS or Give the Location ACCESS to this app")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();


        }
        else {
            trigger=true;
            try {
                locationManager.requestLocationUpdates(locationManager.getBestProvider(criteria, true), 2000, 1, locListener);
            } catch (SecurityException e) {
            }
        }
        btn_zoom=(Button)v.findViewById(R.id.button);
        btn_zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             zoomerButton(v);
            }
        });
        return v;
    }

    private void drawPrimaryLinePath() {
        if (mMap == null) {
            return;
        }


        PolylineOptions option = new PolylineOptions();

        option.add(lastLocation)
                .add(marker.getPosition())
                .color(Color.parseColor("#CC0000FF"))
                .width(10)
                .visible(true);

        mMap.addPolyline(option);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        String providers = locationManager.getBestProvider(criteria, true);
        Location l = null;
        try {

            l = getLastKnownLocation();


             if(l==null){

                 AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                 builder.setMessage("Check the GPS or Give the Location ACCESS to this app")
                         .setCancelable(false)
                         .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int id) {

                             }
                         })
                         .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int id) {
                                 dialog.cancel();
                             }
                         });
                 AlertDialog alert = builder.create();
                 alert.show();
             }
            else {
                 trigger=true;
                 lastLocation = new LatLng(l.getLatitude(), l.getLongitude());

                 //  text.setText("Lat" + i + "= " + l.getLatitude() + "\nLng" + i + "= " + l.getLongitude() + "\n");
                 i++;
                 //LatLng current = new LatLng(l.getLatitude(), l.getLongitude());
                 MarkerOptions options = new MarkerOptions().position(lastLocation).title("Current Location");
                 marker = mMap.addMarker(options);
                 mMap.moveCamera(CameraUpdateFactory.newLatLng(lastLocation));
             }
            }catch(SecurityException e){
            }
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(),9f));

    }

    //public void zoomer(View view) {
    //  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18f));
    //}

    private Location getLastKnownLocation() {
        Location l=null;
        locationManager = (LocationManager) getActivity().getApplicationContext().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            try {
                l = locationManager.getLastKnownLocation(provider);
            }
            catch (SecurityException e) {
            }
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }
        return bestLocation;
    }

    public void zoomerButton(View view) {
         if(trigger)
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18f));
    }


    private void buildAlertMessageNoGps() {
        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }
}



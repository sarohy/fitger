package com.example.awais.acclometer.Ui.Fragments;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.awais.acclometer.R;


public class EnvironmentFragment extends Fragment implements SensorEventListener {
    View v;
    TextView tv_temp;
    private TextView tv_humi;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.environment_f, container, false);
        // Get an instance of the sensor service, and use that to get an instance of
        // a particular sensor.
        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        mTemp = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        mHumidity = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        tv_temp= (TextView) v.findViewById(R.id.txt_temp);
        tv_humi= (TextView) v.findViewById(R.id.txt_humidity);


        return v;
    }
    private SensorManager mSensorManager;
    private Sensor mTemp,mHumidity;



    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        float temp = event.values[0];
        tv_temp.setText(String.valueOf((int) temp));
        double temperature=temp;
        temp = event.values[1];
        double ans=216.8*(((temp/10)*6.112* Math.exp(17.62*temperature/(243.12+temperature)))/(273.15+temperature));
        tv_humi.setText(String.valueOf((int) ans));
        // Do something with this sensor data.
    }

    @Override
    public void onResume() {
        // Register a listener for the sensor.
        super.onResume();
        mSensorManager.registerListener(this, mTemp, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mHumidity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}

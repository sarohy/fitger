package com.example.awais.acclometer.CoreCode;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;


import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.MainActivity;

/**
 * Created by Awais on 11/18/2016.
 */

public class CalculationService extends Service implements SensorEventListener {


    private long lastUpdate = 0;
    public static int steps = 0;
    public static Float calories = 0.0f;
    public int massInPounds = 60;
    public float heightinFeet=0;
    public SensorManager sensorManager;
    public Intent intent;
    Messenger messenger=null;
    SharedPreferences sharedpreferences;
    boolean trigger=false;
    SharedPreferences.Editor editorS;

    @Override
    public void onCreate() {
        super.onCreate();
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        steps=sharedpreferences.getInt("STsteps",0);

    runAsForeground();

    }

    public int onStartCommand(Intent intent, int flags, int startId) {


        calories=0.0f;

            massInPounds = intent.getIntExtra("weight", 60);
            heightinFeet = intent.getFloatExtra("height", 5f);

           if(intent.getBooleanExtra("fromM",false)){
               steps=0;
           }
        else{

           }




        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        this.intent = new Intent();

        Sensor stepcounter = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (stepcounter != null) {
            sensorManager.registerListener(this, stepcounter, SensorManager.SENSOR_DELAY_NORMAL);
            //shownotification();
        } else {
            Toast.makeText(this, "Accellometer Is Not available", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(this, "Service starting", Toast.LENGTH_SHORT).show();

        //SendBroadCast();
       // sendData();

        return START_STICKY;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {

        Sensor mySensor = event.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = (float) event.values[0];
            float y = (float) event.values[1];
            float z = (float) event.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 100) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float length = (y * y + z * z);
                if (length >= 135 && length<=250) {
                    steps++;
                    calories = ((0.5f * massInPounds) / (2000.0f)) * steps;
                     trigger=true;
                    SendBroadCast();


                }


            }
        }
    }


    public void SendBroadCast() {
        intent.setAction("a");
        intent.putExtra("steps", steps);
        intent.putExtra("distance", steps / (5280.0f/(heightinFeet*0.5f)));
        intent.putExtra("calories", calories);
        sendBroadcast(intent);

        /*
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {






                    intent.setAction("a");
                    intent.putExtra("steps", steps);
                    intent.putExtra("distance", steps / (5280.0f/(heightinFeet*0.5f)));
                    intent.putExtra("calories", calories);
                    sendBroadcast(intent);
                   // trigger=false;

                handler.postDelayed(this, 100);
            }

        });
*/
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void shownotification() {
        NotificationManager mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Intent newintent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                newintent, 0);


        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle("FITGER")
                .setTicker("step counting")
                .setContentText("steps Counter")
                .setContentIntent(contentIntent)
                .setOngoing(true)
                .build();
        startForeground(101,
                notification);
        mNM.notify(10, notification);

    }

    public void sendData(){

      final Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
               SendBroadCast();
                handler.postDelayed(this, 100);
            }
        };

        handler.postDelayed(r, 1000);
    }
    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Toast.makeText(getApplicationContext(), "msg Received", Toast.LENGTH_SHORT).show();
            messenger=msg.replyTo;
            super.handleMessage(msg);
        }

    }

    final Messenger msnger = new Messenger(new IncomingHandler());

    public IBinder onBind(Intent intent) {
        return msnger.getBinder();
    }

    private void runAsForeground(){
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent= PendingIntent.getActivity(this, 0,notificationIntent, 0);

        Notification notification=new android.support.v4.app.NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.eicon)
                .setContentTitle("Fitger")
                .setTicker("Walking begins")
                .setContentText("Fitger is counting your steps.")
                .setContentIntent(pendingIntent).build();

        startForeground(1333, notification);

    }

    @Override
    public void onDestroy() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("STsteps", steps);
        editor.apply();
        editor.commit();


        super.onDestroy();
    }

}
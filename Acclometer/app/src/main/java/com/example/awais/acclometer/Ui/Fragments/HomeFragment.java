package com.example.awais.acclometer.Ui.Fragments;

import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.awais.acclometer.CoreCode.BroadCastRecieverforSteps;
import com.example.awais.acclometer.CoreCode.HistoryDbHelper;
import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.MainActivity;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.special.ResideMenu.ResideMenu;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class HomeFragment extends Fragment {

    private View parentView;
    private ResideMenu resideMenu;
    Button ms;
    Button mc;
    Button md;
    SharedPreferences sharedPreferences;
    BarChart barChart,barChart2;
    int [] steps;
    float [] cal;
    public TextView dtext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.home, container, false);
        ms=(Button) parentView.findViewById(R.id.mainSteps);
        mc=(Button) parentView.findViewById(R.id.mainCal);
        md=(Button) parentView.findViewById(R.id.textView11);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());


        steps=new int[7];
        String st=new String();
        Arrays.fill(steps,0);
        st=getData(steps);



        barChart = (BarChart)parentView. findViewById(R.id.mybar3);

        dtext=(TextView)parentView.findViewById(R.id.dtext2);
        dtext.setText(st);

        ArrayList<BarEntry> barEntries = new ArrayList<>();


        barEntries.add(new BarEntry(1,steps[0]));
        barEntries.add(new BarEntry(2,steps[1]));
        barEntries.add(new BarEntry(3,steps[2]));
        barEntries.add(new BarEntry(4,steps[3]));
        barEntries.add(new BarEntry(5,steps[4]));
        barEntries.add(new BarEntry(6,steps[5]));
        barEntries.add(new BarEntry(7,steps[6]));




        BarDataSet barDataSet = new BarDataSet(barEntries,"steps Per Day");
        barDataSet.setBarShadowColor(Color.GREEN);
        barDataSet.setColor(Color.YELLOW);





        BarData barData = new BarData(barDataSet);
        barDataSet.setValueTextColor(Color.WHITE);
        barChart.setData(barData);





        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
        barChart.animateXY(5000,5000);

        Legend l = barChart.getLegend();
               l.setDrawInside(false);
               l.setTextColor(Color.WHITE);




        YAxis yAxis = barChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawAxisLine(false);
        yAxis.setTextColor(Color.WHITE);

        YAxis y2Axis = barChart.getAxisLeft();
        y2Axis.setDrawGridLines(false);
        y2Axis.setDrawAxisLine(false);
        y2Axis.setTextColor(Color.WHITE);




        XAxis xAxis=barChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(Color.WHITE);






        setUpViews();
        setdata();



        return parentView;
    }

    private void setUpViews() {
        MainActivity parentActivity = (MainActivity) getActivity();
        resideMenu = parentActivity.getResideMenu();
    }


    public void setdata(){
       final int w=sharedPreferences.getInt("weight",60);
        final float h=sharedPreferences.getFloat("height",5f);

        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {

                    int s = sharedPreferences.getInt("tlsteps",0);
                    Float cal = ((0.5f * w) / (2000.0f)) * s;
                    Float dist = s / (5280.0f/(h*0.5f));
                    ms.setText("Today\n "+String.valueOf(s));
                    mc.setText("Cal.\n "+String.valueOf(round(cal,1)));
                    md.setText("Miles\n "+String.valueOf(round(dist,2)));



                handler.postDelayed(this, 100);
            }

        });


    }

    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }


    public String getData(int[]s) {

        String st=new String();
        st="";

        HistoryDbHelper helper = new HistoryDbHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        String query = "SELECT * FROM history ORDER BY _ID DESC LIMIT 7";
        Cursor cursor = db.rawQuery(query, null);
        Cursor cursor2 = db.rawQuery(query, null);
        int i = 0;
        while (cursor.moveToNext()) {

            s[i] = cursor.getInt(cursor.getColumnIndex("steps"));
            if(i==0)
                st=cursor.getString(cursor.getColumnIndex("date"));

            i++;
            cursor2.moveToNext();
        }
        if(i==0)
            st="No Data Available";
        else
            st=st+" to "+cursor2.getString(cursor.getColumnIndex("date"));

        return st;
    }


}

package com.example.awais.acclometer;

import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.TextView;

import com.special.ResideMenu.ResideMenu;

public class swimming extends Fragment {
    TextView timer;
    TextView cal;
    int modValue;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedTime = 0L;
    private long startTime = 0L;
    private Handler customHandler = new Handler();
    int count;


   private Runnable updateTimerThread = new Runnable() {
       public void run() {
           timeInMilliseconds = SystemClock.uptimeMillis() - startTime;
           updatedTime = timeSwapBuff + timeInMilliseconds;
           int secs = (int) (updatedTime / 1000);
           if (secs % modValue == 0) {
               count++;
               cal.setText("Cal=" + count);
           }
           int mins = secs / 60;

           secs = secs % 60;

           int milliseconds = (int) (updatedTime % 1000);
           timer.setText("" + mins + ":"
                   + String.format("%02d", secs) );


           customHandler.postDelayed(this, 1000);
       }
   };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v=inflater.inflate(R.layout.activity_swimming, container, false);

        timer=(TextView)v.findViewById(R.id.textView4);
        cal=(TextView)v.findViewById(R.id.textView5);
        modValue=5;
        timeSwapBuff += timeInMilliseconds;
        customHandler.removeCallbacks(updateTimerThread);
        customHandler.removeCallbacks(updateTimerThread);
        timeSwapBuff=0l;
        timeInMilliseconds=0l;
        updatedTime=0l;
      v.findViewById(R.id.startswim).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button button=(Button) view;
                if(button.getText().equals("Stop")){
                    customHandler.removeCallbacks(updateTimerThread);
                    timeSwapBuff=0l;
                    timeInMilliseconds=0l;
                    updatedTime=0l;
                    button.setText("Start");
                }

                else {
                    button.setText("Stop");
                    startTime = SystemClock.uptimeMillis();
                    customHandler.postDelayed(updateTimerThread, 0);
                }

            }
        });
return v;
    }



}

package com.example.awais.acclometer.Ui;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.example.awais.acclometer.Ui.Fragments.Login;
import com.example.awais.acclometer.sharetoFB;
import com.facebook.FacebookSdk;


import com.example.awais.acclometer.CoreCode.GoogleMaps;
import com.example.awais.acclometer.CoreCode.RunningTracker;
import com.example.awais.acclometer.CoreCode.speedoMeter;
import com.example.awais.acclometer.CoreCode.webview;
import com.example.awais.acclometer.Ui.Fragments.EnvironmentFragment;
import com.example.awais.acclometer.Ui.Fragments.ExerciseFragment;
import com.example.awais.acclometer.Ui.Fragments.HomeFragment;
import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.Fragments.ProfileFragment;
import com.example.awais.acclometer.Ui.Fragments.SettingsFragment;
import com.example.awais.acclometer.tutorials;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

public class MainActivity extends FragmentActivity implements View.OnClickListener, RunningTracker.RunningListener {

    private ResideMenu resideMenu;
    private MainActivity mContext;
    private ResideMenuItem itemHome;
    private ResideMenuItem speedometer;
    private ResideMenuItem location;
    private ResideMenuItem itemProfile;
    private ResideMenuItem itemExercise;
    private ResideMenuItem itemEnvironment;
    private ResideMenuItem itemSettings;
    private ResideMenuItem googlesearch;
    private ResideMenuItem tutorials;
    private ResideMenuItem share;
    private ResideMenuItem logout;

    Fragment fragment;
    HomeFragment hf;
    private SharedPreferences sharedpreferences;
    int count = 0;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        final AdView adView = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder()
                .setRequestAgent("android_studio:ad_template").build();
         adView.loadAd(adRequest);
        adView.setAdListener(new AdListener() {

            @Override
            public void onAdLoaded() {
                adView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAdFailedToLoad(int error) {
                adView.setVisibility(View.GONE);
            }

        });



        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(sharedpreferences.getBoolean("logout",false)){
            Intent intent=new Intent(this, Login.class);
            startActivity(intent);
            finish();

        }


        mContext = this;
        hf = new HomeFragment();
        setUpMenu();
        if (savedInstanceState == null)
            changeFragment(new HomeFragment());

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            boolean value = extras.getBoolean("timeService",false);
            if (value) {
                changeFragment(new ExerciseFragment());
            }
            boolean ans = extras.getBoolean("profile");
            if (ans) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("done", true);
                editor.apply();
                editor.commit();
                changeFragment(new ProfileFragment());
            }
            //The key argument here must match that used in the other activity
        }
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean("hogya", true);
        editor.apply();
        editor.commit();
    }

    private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);

        resideMenu.setBackground(R.drawable.menuimg);
        resideMenu.attachToActivity(this);
        // resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
        resideMenu.setScaleValue(0.6f);

        // create menu items;
        itemHome = new ResideMenuItem(this, R.drawable.home, "Home");
        itemProfile = new ResideMenuItem(this, R.drawable.uicon, "Profile");
        itemExercise = new ResideMenuItem(this, R.drawable.eicon, "Exercise Mate");
        speedometer = new ResideMenuItem(this, R.drawable.speedo, "Speedo  Meter");
        itemEnvironment = new ResideMenuItem(this, R.drawable.barometericon, "Environment");
        itemSettings = new ResideMenuItem(this, R.drawable.sicon50, "Settings");
        location = new ResideMenuItem(this, R.drawable.maps, "Location");
        googlesearch = new ResideMenuItem(this, R.drawable.googleicon, "google Search");
        tutorials = new ResideMenuItem(this, R.drawable.tutorials, "Health Tutorials");
        share = new ResideMenuItem(this, R.drawable.fb, "Share");
        logout = new ResideMenuItem(this, R.drawable.fb, "Logout");

        itemHome.setOnClickListener(this);
        speedometer.setOnClickListener(this);
        itemProfile.setOnClickListener(this);
        itemExercise.setOnClickListener(this);
        itemEnvironment.setOnClickListener(this);
        itemSettings.setOnClickListener(this);
        location.setOnClickListener(this);
        googlesearch.setOnClickListener(this);
        tutorials.setOnClickListener(this);
        share.setOnClickListener(this);
        logout.setOnClickListener(this);


        resideMenu.addMenuItem(itemHome, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemProfile, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemExercise, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(speedometer, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemEnvironment, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);

        resideMenu.addMenuItem(location, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(googlesearch, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(tutorials, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(share, ResideMenu.DIRECTION_RIGHT);
        resideMenu.addMenuItem(logout, ResideMenu.DIRECTION_RIGHT);
        // You can disable a direction by setting ->
        // resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);

        findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        findViewById(R.id.title_bar_right_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemHome) {
            changeFragment(new HomeFragment());
        } else if (view == itemProfile) {
            changeFragment(new ProfileFragment());
        } else if (view == itemExercise) {
            changeFragment(new ExerciseFragment());
        } else if (view == itemSettings) {
            changeFragment(new SettingsFragment());
        } else if (view == itemEnvironment) {
            changeFragment(new EnvironmentFragment());
        } else if (view == speedometer) {
            changeFragment(new speedoMeter());
        } else if (view == location) {
            changeFragment(new GoogleMaps());
        } else if (view == googlesearch) {
            changeFragment(new webview());
        } else if (view == tutorials) {
            changeFragment(new tutorials());
        }
        else if (view == share) {
         //   changeFragment(new tutorials());
            Intent intent=new Intent(this,sharetoFB.class);
            startActivity(intent);

        }
        else if (view == logout) {
            //   changeFragment(new tutorials());

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("done", false);
            editor.putBoolean("logout",true);
            editor.putBoolean("hogya",false);
            editor.apply();
            editor.commit();

            Intent intent=new Intent(this, Login.class);
            startActivity(intent);
        }


        resideMenu.closeMenu();
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {

        }

        @Override
        public void closeMenu() {

        }
    };

    private void changeFragment(Fragment targetFragment) {
        fragment = targetFragment;
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu() {
        return resideMenu;
    }

    @Override
    public void launchHistory(String savetoo, String l1, int i, String date, Float caloriesfromBroadCast, Float distfromBroadCast) {

    }

    @Override
    public void onBackPressed() {


        if (getFragmentManager().getBackStackEntryCount() > 1) {
            FragmentManager transaction = getFragmentManager();
            transaction.popBackStackImmediate();
        } else {
             /*
            if(hf.isVisible()==true){
                super.onBackPressed();
            }
            else {
                changeFragment(hf);
            }
        }
*/
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("fromMain", true);
            editor.apply();
            editor.commit();
            super.onBackPressed();
        }

    }
}
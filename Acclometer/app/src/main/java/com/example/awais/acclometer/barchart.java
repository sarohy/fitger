package com.example.awais.acclometer;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

import com.example.awais.acclometer.CoreCode.History;
import com.example.awais.acclometer.CoreCode.HistoryDbHelper;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineDataSet;

public class barchart extends Fragment {

    BarChart barChart,barChart2;
    int [] steps;
    float [] cal;
 public TextView dtext;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_barchart,container,false);



        steps=new int[7];
        cal=new float[7];
        String st=new String();
        Arrays.fill(steps,0);
        Arrays.fill(cal,0f);
        st=getData(steps,cal);



        barChart = (BarChart)v. findViewById(R.id.mybar);
        barChart2 = (BarChart)v. findViewById(R.id.mybar2);
        dtext=(TextView)v.findViewById(R.id.dtext);
        dtext.setText(st);

        ArrayList<BarEntry> barEntries = new ArrayList<>();


        barEntries.add(new BarEntry(1,steps[0]));
        barEntries.add(new BarEntry(2,steps[1]));
        barEntries.add(new BarEntry(3,steps[2]));
        barEntries.add(new BarEntry(4,steps[3]));
        barEntries.add(new BarEntry(5,steps[4]));
        barEntries.add(new BarEntry(6,steps[5]));
        barEntries.add(new BarEntry(7,steps[6]));

        ArrayList<BarEntry> barEntries2 = new ArrayList<>();
        barEntries2.add(new BarEntry(1,cal[0]));
        barEntries2.add(new BarEntry(2,cal[1]));
        barEntries2.add(new BarEntry(3,cal[2]));
        barEntries2.add(new BarEntry(4,cal[3]));
        barEntries2.add(new BarEntry(5,cal[4]));
        barEntries2.add(new BarEntry(6,cal[6]));



        BarDataSet barDataSet = new BarDataSet(barEntries,"steps Per Day");
        barDataSet.setBarShadowColor(Color.GREEN);
        barDataSet.setColor(Color.BLUE);


        BarDataSet barDataSet2 = new BarDataSet(barEntries2,"Calories Burnt Per Day");
        barDataSet2.setBarShadowColor(Color.GREEN);
        barDataSet2.setColor(Color.RED);




        BarData barData = new BarData(barDataSet);
        barChart.setData(barData);


        BarData barData2 = new BarData(barDataSet2);
        barChart2.setData(barData2);

        barChart.setTouchEnabled(true);
        barChart.setDragEnabled(true);
        barChart.setScaleEnabled(true);
        barChart.animateXY(5000,5000);

        barChart2.setTouchEnabled(true);
        barChart2.setDragEnabled(true);
        barChart2.setScaleEnabled(true);
        barChart2.animateXY(5000,5000);

        YAxis yAxis = barChart.getAxisRight();
        yAxis.setDrawGridLines(false);
        yAxis.setDrawAxisLine(false);


        YAxis y2Axis = barChart.getAxisLeft();
        y2Axis.setDrawGridLines(false);
        y2Axis.setDrawAxisLine(false);



        XAxis xAxis=barChart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);

///bar chart 2
        YAxis yAxis2 = barChart2.getAxisRight();
        yAxis2.setDrawGridLines(false);
        yAxis2.setDrawAxisLine(false);


        YAxis y2Axis2 = barChart2.getAxisLeft();
        y2Axis2.setDrawGridLines(false);
        y2Axis2.setDrawAxisLine(false);



        XAxis xAxis2=barChart2.getXAxis();
        xAxis2.setDrawGridLines(false);
        xAxis2.setDrawAxisLine(false);






        return v;
    }

    public String getData(int[]s,float []c) {

        String st=new String();
        st="";

        HistoryDbHelper helper = new HistoryDbHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        String query = "SELECT * FROM history ORDER BY _ID DESC LIMIT 7";
        Cursor cursor = db.rawQuery(query, null);
        Cursor cursor2 = db.rawQuery(query, null);
        int i = 0;
        while (cursor.moveToNext()) {

            s[i] = cursor.getInt(cursor.getColumnIndex("steps"));
            if(i==0)
            st=cursor.getString(cursor.getColumnIndex("date"));

            c[i]=cursor.getFloat(cursor.getColumnIndex("calories"));
            i++;
            cursor2.moveToNext();
        }
        if(i==0)
            st="No Data Available";
        else
            st=st+" to "+cursor2.getString(cursor.getColumnIndex("date"));

        return st;
    }

}

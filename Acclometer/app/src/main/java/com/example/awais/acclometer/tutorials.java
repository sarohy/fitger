package com.example.awais.acclometer;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class tutorials extends Fragment {
    WebView myWebview;
    ProgressBar pb;
    int ProgrusStatus=4;
    Button refresh;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_tutorials,container,false);

        myWebview=(WebView)v.findViewById(R.id.mywebview2);
        myWebview.setVisibility(getView().INVISIBLE);
        pb=(ProgressBar)v.findViewById(R.id.progressBar2);
        refresh=(Button)v.findViewById(R.id.refresh);
        pb.setVisibility(View.INVISIBLE);

        myWebview.getSettings().setJavaScriptEnabled(true);

        myWebview.getSettings().setJavaScriptEnabled(true);

        final Activity activity =getActivity();

        myWebview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                pb.setProgress(ProgrusStatus);
                ProgrusStatus=ProgrusStatus+8;
                //activity.setProgress(progress * 1000);

            }


        });
        myWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, "Error! " + description, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onPageFinished(WebView view, String url) {
                ProgrusStatus=0;
                pb.setVisibility(View.INVISIBLE);
                myWebview.setVisibility(getView().VISIBLE);

            }
        });
        String sHtmlTemplate = "<html><head></head><body><img src=\"file:///android_asset/logo4.jpg\"></body></html>";

        myWebview.loadDataWithBaseURL(null, sHtmlTemplate, "text/html", "utf-8",null);


        pb.setVisibility(View.VISIBLE);
        pb.setProgress(4);
        myWebview.loadUrl("https://www.youtube.com/results?search_query=health+tutorials");

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pb.setVisibility(View.VISIBLE);
                pb.setProgress(4);
                myWebview.loadUrl("https://www.youtube.com/results?search_query=health+tutorials");


            }
        });
return v;
    }


}

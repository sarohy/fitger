package com.example.awais.acclometer.CoreCode;

import android.app.Activity;
import android.app.ActivityManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

//import com.google.android.gms.appindexing.Action;
//import com.google.android.gms.appindexing.AppIndex;
//import com.google.android.gms.appindexing.Thing;
//import com.google.android.gms.common.api.GoogleApiClient;

import com.example.awais.acclometer.R;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class RunningTracker extends Fragment {





///////////////
    SharedPreferences sharedpreferences;
    boolean started;
    public TextView counterText;
    public TextView caloriesText;
    public TextView distanceText;
    Button button;
    public int weight;
    public Float heightInFeet;
    boolean isBound=false;
    Calendar c2;
    String CurrDate;
    View v;

    private Context context;
    RunningListener listener;

    public interface RunningListener
    {
        public void launchHistory(String savetoo, String l1, int i, String date, Float caloriesfromBroadCast, Float distfromBroadCast);
    }

    public void onAttach(Activity activity) {
        this.context=activity;;
        listener= (RunningListener) activity;
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.running_tracker,container,false);
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        heightInFeet=sharedpreferences.getFloat("height",5f);
        weight=sharedpreferences.getInt("weight",60);

        isBound=sharedpreferences.getBoolean("isbound",false);
        if (sharedpreferences.contains("IsStart"))
            started = sharedpreferences.getBoolean("IsStart", false);

        else
            started = false;

        //setContentView(R.layout.running_tracker);
        counterText = (TextView) v.findViewById(R.id.counter);
        caloriesText = (TextView) v.findViewById(R.id.caloriesText);
        distanceText = (TextView) v.findViewById(R.id.distanceText);
        button = (Button) v.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartButton(v);
            }
        });

        if(!getActivity().getIntent().getBooleanExtra("timeservice",false)) {
            if (started) {
                button.setText("Stop Running");
                setSteps();
            } else
                button.setText("Start Running");
        }
        else if(getActivity().getIntent().getBooleanExtra("timeservice",false)){

            //////////changings'
            if(isMyServiceRunning(TimeService.class)|| started==true) {
                SharedPreferences.Editor editor2 = sharedpreferences.edit();
                editor2.putBoolean("reminder", false);
                // editor2.putBoolean("isStart", true);
                editor2.apply();
                editor2.commit();

                Intent serviceIntent = new Intent(getContext(), TimeService.class);
                getActivity().stopService(serviceIntent);
                TimeSchedule_Activity.resetButtonText = true;

                if(started==false) {
                    button.setText("Start Running");
                    StartButton(button);
                }
                else{
                    button.setText("Stop Running");
                    setSteps();
                }
                // setSteps();
            }
            else{
                button.setText("Start Running");started=false;
            }


        }

        return v;
    }

    public void StartButton(View view) {

        if (button.getText().equals("Start Running")) {
            started = true;

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("IsStart", started);
            editor.apply();
            editor.commit();

            Intent intent = new Intent(getActivity(), CalculationService.class);
            intent.putExtra("weight",weight);
            intent.putExtra("fromM",true);
            intent.putExtra("height",heightInFeet);
            getActivity().startService(intent);

            isBound=true;


            setSteps();

            button.setText("Stop Running");
        } else {
            button.setText("Start Running");
            started = false;

            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putBoolean("IsStart", started);
            editor.apply();
            editor.commit();


           Intent serviceIntent = new Intent(getContext(), CalculationService.class);
            getActivity().stopService(serviceIntent);

            isBound=false;


           //BroadCastRecieverforSteps.steps=0;
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            String date = df.format(c.getTime());
            //listener.launchHistory("savetoo", "l1",BroadCastRecieverforSteps.getstepfromBroadCast(),date, BroadCastRecieverforSteps.getCaloriesfromBroadCast(),BroadCastRecieverforSteps.getDistfromBroadCast());

            FragmentManager fragMan = getFragmentManager();
           FragmentTransaction fragTransaction = fragMan.beginTransaction();
            HistoryActivity myFrag = new HistoryActivity();

            int s=BroadCastRecieverforSteps.getstepfromBroadCast();
            float cal=((0.5f * weight) / (2000.0f)) * BroadCastRecieverforSteps.getstepfromBroadCast();
            float dist=BroadCastRecieverforSteps.getstepfromBroadCast()/ (5280.0f/(heightInFeet*0.5f));

            myFrag.update("savetoo","l1",BroadCastRecieverforSteps.getstepfromBroadCast(),date.toString(), cal,dist);
            BroadCastRecieverforSteps.steps=0;
            fragTransaction.replace(R.id.main_fragment, myFrag , "runningTracker");                       //cal   //dist
            fragTransaction.addToBackStack(null);
            fragTransaction.commit();


        }



    }

    public void setSteps() {

      // BroadCastRecieverforSteps.steps=0;
       // BroadCastRecieverforSteps.cal=0.0f;
        //BroadCastRecieverforSteps.dist=0.0f;
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (started) {
                    int s = BroadCastRecieverforSteps.getstepfromBroadCast();
                    Float cal = ((0.5f * weight) / (2000.0f)) * s;//BroadCastRecieverforSteps.getCaloriesfromBroadCast();
                    Float dist = s / (5280.0f/(heightInFeet*0.5f));//BroadCastRecieverforSteps.getDistfromBroadCast();

                    counterText.setText(String.valueOf( BroadCastRecieverforSteps.steps));
                    distanceText.setText(String.valueOf(round(dist,1)));
                    caloriesText.setText(String.valueOf(round(cal,2)));


                }
                handler.postDelayed(this, 100);
            }

        });

    }

    public void showHistory(View view) {
        Intent historyIntent = new Intent(getActivity(), HistoryActivity.class);
        historyIntent.putExtra("check", "onlyshow");
        historyIntent.putExtra("loadfrom", "l1");
        startActivity(historyIntent);

    }

    @Override
    public void onPause() {

        super.onPause();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }


    public void setScheduale(View view) {   // this is a onclick function
        Intent intent=new Intent(getContext(),TimeSchedule_Activity.class);
        startActivity(intent);
    }

    public void showGoogleMaps(View view) { //this is a on click  function
        Intent intent=new Intent(getContext(),GoogleMaps.class);
        startActivity(intent);
    }


    public void openSpeedoMeter(View view) {//this is a on click  function
        Intent intent=new Intent(getContext(),speedoMeter.class);
        startActivity(intent);
    }

    public void ShowHistory2(View view) {
        Intent historyintent = new Intent(getContext(), HistoryActivity.class);
        historyintent.putExtra("check", "onlyshow");
        historyintent.putExtra("loadfrom", "l2");
        startActivity(historyintent);

    }


}

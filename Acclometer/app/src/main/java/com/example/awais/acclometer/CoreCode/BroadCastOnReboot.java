package com.example.awais.acclometer.CoreCode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Awais on 12/1/2016.
 */

public class BroadCastOnReboot extends BroadcastReceiver {
    SharedPreferences sharedPreferences;
    @Override
    public void onReceive(Context context, Intent intent) {
        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        if(sharedPreferences.getBoolean("reminder",false)){
            //starting time service
            Intent intent1=new Intent(context,TimeService.class);
            intent1.putExtra("fromSC",false);
            context.startService(intent1);
        }
        if(sharedPreferences.getBoolean("isStart",false)){
            //startiing caclculation service
            Intent intent1=new Intent(context,CalculationService.class);
            intent.putExtra("fromM",false);
            intent.putExtra("height",sharedPreferences.getFloat("height",5f));
            intent.putExtra("height",sharedPreferences.getInt("weight",60));
            context.startService(intent1);

        }

    }
}

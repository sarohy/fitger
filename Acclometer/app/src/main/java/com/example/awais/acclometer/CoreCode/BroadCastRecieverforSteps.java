package com.example.awais.acclometer.CoreCode;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Awais on 11/18/2016.
 */

public class BroadCastRecieverforSteps extends BroadcastReceiver{

    public static int steps=0;
    public static int totalsteps=0;
    public static Float dist=0.00f;
    public static Float cal=0.00f;
    SharedPreferences sharedPreferences;



    @Override
    public void onReceive(Context context, Intent intent) {
         sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

       steps=intent.getIntExtra("steps",0);
       // steps=steps+1;
        totalsteps=sharedPreferences.getInt("tlsteps",0)+1;
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("tlsteps", totalsteps);
        editor.apply();
        editor.commit();

        //  dist=intent.getFloatExtra("distance",0.0f);
        //cal=intent.getFloatExtra("calories",0.00f);

        Toast.makeText(context,"Receiving",Toast.LENGTH_SHORT).show();

    }

    public static int getstepfromBroadCast(){
        return steps;
    }
    public static int getTotalstepsstepfromBroadCast(){
        return totalsteps;
    }

    public static Float getCaloriesfromBroadCast(){return cal;}
    public static Float getDistfromBroadCast(){return dist;}


}

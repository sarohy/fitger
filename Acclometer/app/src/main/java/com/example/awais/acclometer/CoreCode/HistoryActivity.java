package com.example.awais.acclometer.CoreCode;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.awais.acclometer.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

public class HistoryActivity extends Fragment {

    public ListView listView;
    public AdapterforHistory adapterforHistory;
    public ArrayList<History> list2 =new ArrayList();
    int index;
    View v;
    private String savetoo="",l1="",s="";
    private float caloriesfromBroadCast=0,distfromBroadCast=0;
    private int i=0;
    Button shrbtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {

       super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        index=0;
        if(!savetoo.equals("onlyshow")) {
            //  float f=getIntent().getFloatExtra("calories",0.0f);
            SaveHistory(i,caloriesfromBroadCast,distfromBroadCast,s);
        }

        if(l1.equals("l2"))
            LoadHistory2();
        else
            LoadHistory();

        v=inflater.inflate(R.layout.activity_history,container,false);
        listView=(ListView)v.findViewById(R.id.historylist);
        shrbtn=(Button)v.findViewById(R.id.shrbtn);
        adapterforHistory=new AdapterforHistory(getActivity(),R.layout.history_list_items,list2);
        listView.setAdapter(adapterforHistory);
       shrbtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v1) {
               shareit(v);

           }
       });
        return v;
    }

    public void LoadHistory() {
        HistoryDbHelper helper = new HistoryDbHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();

        String query = "SELECT * FROM history ORDER BY _ID DESC";
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {

            //list[index].LoadData(cursor);
            History hist = new History();
            hist.LoadData(cursor);
            list2.add(hist);
            index++;

        }
    }

    public void LoadHistory2(){  // to load data of whole day
        HistoryDbHelper helper=new HistoryDbHelper(getActivity());
        SQLiteDatabase db=helper.getReadableDatabase();

        String query = "SELECT * FROM history2 ORDER BY _ID DESC";
        Cursor cursor = db.rawQuery(query,null);
        while(cursor.moveToNext()) {

            //list[index].LoadData(cursor);
            History hist=new History();
            hist.LoadData(cursor);
            list2.add(hist);
            index++;

        }


    }
    public void SaveHistory(int steps,Float calories,Float distance,String Date){
        HistoryDbHelper helper=new HistoryDbHelper(getActivity());
        SQLiteDatabase db=helper.getWritableDatabase();
        History.saveinDb(db,steps,distance,calories,Date);

    }


    public void update(String savetoo, String l1, int i, String s, Float caloriesfromBroadCast, Float distfromBroadCast)
    {
        this.savetoo=savetoo;
        this.l1=l1;
        this.i=i;
        this.s=s;
        this.caloriesfromBroadCast=caloriesfromBroadCast;
        this.distfromBroadCast=distfromBroadCast;

    }

    public void setSimple(String savetoo, String l1)
    {
        this.savetoo=savetoo;
        this.l1=l1;
    }


    public void shareit(View view)
    {
      //  View view =  findViewById(R.id.layout);//your layout id
        view.getRootView();
        File picFile=new File("hello");
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state))
        {
            File picDir  = new File(Environment.getExternalStorageDirectory()+ "/myPic");
            if (!picDir.exists())
            {
                picDir.mkdir();
            }
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache(true);
            Bitmap bitmap = view.getDrawingCache();
//          Date date = new Date();
            String fileName = "mylove" + ".jpg";

            picFile = new File(picDir + "/" + fileName);
            try
            {
                picFile.createNewFile();
                FileOutputStream picOut = new FileOutputStream(picFile);
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), (int)(bitmap.getHeight()/1.2));
                boolean saved = bitmap.compress(Bitmap.CompressFormat.JPEG, 100, picOut);
                if (saved)
                {
                    Toast.makeText(getActivity(), "Image saved to your device Pictures "+ "directory!", Toast.LENGTH_SHORT).show();
                } else
                {
                    //Error
                }
                picOut.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            view.destroyDrawingCache();
        } else {
            //Error

        }




        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("image/jpeg");

        sharingIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(picFile.getAbsolutePath()));
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }
}

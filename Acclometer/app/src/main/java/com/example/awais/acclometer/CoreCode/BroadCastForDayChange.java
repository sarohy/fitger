package com.example.awais.acclometer.CoreCode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Awais on 12/1/2016.
 */public class BroadCastForDayChange extends BroadcastReceiver {

    public Context context;


    @Override
    public void onReceive(Context context, Intent intent) {
          this.context=context;
        save();
        Toast.makeText(context, "Data Saved", Toast.LENGTH_SHORT).show();

    }


    public void save(){
        Float heightInFeet;
        int weight;
        String Date;
        SharedPreferences sharedpreferences;
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        heightInFeet=sharedpreferences.getFloat("height",5f);
        weight=sharedpreferences.getInt("weight",60);
        Calendar c2;
        c2 = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        c2.add(Calendar.DATE,-1);
       Date = df.format(c2.getTime());

        int s=sharedpreferences.getInt("tlsteps",0);
        Float cal=((0.5f * weight) / (2000.0f))*s;
        Float dist=s/(5280.0f/(heightInFeet*0.5f));
        Save_on_DayBasis(s,cal,dist, Date);

        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt("tlsteps", 0);
        editor.apply();
        editor.commit();
        BroadCastRecieverforSteps.totalsteps=0;
    }


    public void Save_on_DayBasis(int steps,Float calories,Float distance,String Date){

        HistoryDbHelper helper=new HistoryDbHelper(context);
        SQLiteDatabase db=helper.getWritableDatabase();
        History.saveinDb2(db,steps,distance,calories,Date);



    }


}

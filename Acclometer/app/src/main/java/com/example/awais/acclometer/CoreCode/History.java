package com.example.awais.acclometer.CoreCode;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.Calendar;

/**
 * Created by Awais on 11/19/2016.
 */

public class History {
    int id;
    int Steps;
    Float distance;
    Float caloriesBurnt;
    String Date;
    String Time;

    public History(){

    }
    public History(int steps, Float distance, Float caloriesBurnt ,String date) {
        Steps = steps;
        this.distance = distance;
        Date = date;
        this.caloriesBurnt = caloriesBurnt;
    }

public static void saveinDb(SQLiteDatabase db,int steps,Float distance,Float caloriesBurnt,String Date){

        ContentValues values=new ContentValues();
        values.put("steps",steps);
        values.put("distance",distance);
        values.put("calories",caloriesBurnt);
        values.put("date",Date);
        Calendar c = Calendar.getInstance();
        int Hr24=c.get(Calendar.HOUR_OF_DAY);
        int Min=c.get(Calendar.MINUTE);
        values.put("time","Time: "+Hr24+":"+Min);
        db.insertWithOnConflict("history",null,values,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static void saveinDb2(SQLiteDatabase db,int steps,Float distance,Float caloriesBurnt,String Date){

        ContentValues values=new ContentValues();
        values.put("steps",steps);
        values.put("distance",distance);
        values.put("calories",caloriesBurnt);
        values.put("date",Date);
        Calendar c = Calendar.getInstance();

        values.put("time","Time:24 Hours");
        db.insertWithOnConflict("history2",null,values,SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void LoadData(Cursor cursor){
        this.Steps=cursor.getInt(cursor.getColumnIndex("steps"));
        this.distance=cursor.getFloat(cursor.getColumnIndex("distance"));
        this.caloriesBurnt=cursor.getFloat(cursor.getColumnIndex("calories"));
        this.Date=cursor.getString(cursor.getColumnIndex("date"));
        this.Time=cursor.getString(cursor.getColumnIndex("time"));
        this.id=cursor.getInt(cursor.getColumnIndex("_ID"));

    }

    public int getSteps() {
        return Steps;
    }

    public void setSteps(int steps) {
        Steps = steps;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Float getCaloriesBurnt() {
        return caloriesBurnt;
    }

    public void setCaloriesBurnt(Float caloriesBurnt) {
        this.caloriesBurnt = caloriesBurnt;
    }

    public String getDate() {
        return Date;
    }
    public String getTime() {
        return Time;
    }

    public void setDate(String date) {
        Date = date;
    }
    public int getId(){
        return id;
    }
}

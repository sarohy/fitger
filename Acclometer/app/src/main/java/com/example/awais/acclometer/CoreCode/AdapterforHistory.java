package com.example.awais.acclometer.CoreCode;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.awais.acclometer.R;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Awais on 11/19/2016.
 */

public class AdapterforHistory extends ArrayAdapter<History>
{
    private Context context;
    private int layoutResourceId;
    private ArrayList<History> data = new ArrayList<History>();

    public AdapterforHistory(Context context, int resource, ArrayList<History> data) {
        super(context, resource,data);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = data;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.date = (TextView) row.findViewById(R.id.dateText);
            holder.steps = (TextView) row.findViewById(R.id.stepText);
            holder.distance = (TextView) row.findViewById(R.id.distanceText);
            holder.calories=(TextView)row.findViewById(R.id.caloriesText);
            holder.delete_btn=(Button)row.findViewById(R.id.DeleteButton) ;
            holder._id=(TextView)row.findViewById(R.id._IDText);
            holder.Time=(TextView)row.findViewById(R.id.timetext);

            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        holder.delete_btn.setTag(position);

        final History item = data.get(position);

        holder.date.setText(String.valueOf(item.getDate()));
        holder.steps.setText("Steps:"+String.valueOf(item.getSteps()));
        holder.distance.setText("Distance(M):"+String.valueOf(round(item.getDistance(),2)));
        holder.calories.setText("CaloriesBurnt:"+String.valueOf(round(item.getCaloriesBurnt(),2)));
        holder._id.setText("RunNO:"+String.valueOf(item.getId()));
        holder.Time.setText(String.valueOf(item.getTime()));

        holder.delete_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final Integer index = (Integer) v.getTag();


                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure you want to Delete this Record?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                removefromdb(data.get(index.intValue()).getId());
                                data.remove(index.intValue());


                                notifyDataSetChanged();









                                //////
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();






            }
        });



        return row;
    }
    static class ViewHolder {
        TextView date;
        TextView steps;
        TextView calories;
        TextView distance;
        TextView Time;
        TextView _id;
        Button delete_btn;
    }

    public void removefromdb(int id){
        HistoryDbHelper helper=new HistoryDbHelper(context);
        SQLiteDatabase db=helper.getWritableDatabase();
        db.delete("history", "_ID = ?", new String[] { String.valueOf(id) });


    }
    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }




}

package com.example.awais.acclometer.Ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.awais.acclometer.LoginFragment;
import com.example.awais.acclometer.R;
import com.example.awais.acclometer.Ui.Fragments.Login;
import com.example.awais.acclometer.Ui.Fragments.Signup;
import com.facebook.FacebookSdk;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class tempactivitey extends AppCompatActivity {
    public static boolean fromMain=false;
    SharedPreferences sharedPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_tempactivitey);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(sharedPreferences.getBoolean("hogya",false)&& !sharedPreferences.getBoolean("fromMain",false)){

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if(sharedPreferences.getBoolean("hogya",false)&& sharedPreferences.getBoolean("fromMain",false)){
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("fromMain", false);
            editor.apply();
            editor.commit();
            finish();
        }

        }

    @Override
    protected void onStop() {

        super.onStop();


    }
    /*
    if(sharedPreferences.getBoolean("logresult",false) && !sharedPreferences.getBoolean("fromMain",false))
    {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
    }
        else if(sharedPreferences.getBoolean("logresult",false) && sharedPreferences.getBoolean("fromMain",false)){

        finish();
    }
*/


}

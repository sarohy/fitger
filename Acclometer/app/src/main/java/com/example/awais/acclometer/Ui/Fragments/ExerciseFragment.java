package com.example.awais.acclometer.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.awais.acclometer.CoreCode.GoogleMaps;
import com.example.awais.acclometer.CoreCode.History;
import com.example.awais.acclometer.CoreCode.HistoryActivity;
import com.example.awais.acclometer.CoreCode.RunningTracker;
import com.example.awais.acclometer.CoreCode.TimeSchedule_Activity;
import com.example.awais.acclometer.R;
import com.example.awais.acclometer.barchart;
import com.example.awais.acclometer.swimming;

public class ExerciseFragment extends Fragment  {

    private View parentView;
    private Button btn_start, btn_schedule;
    private Button btn_history;
    private Button btn_dailyHistory;
    private Button btn_chart,btn_swim;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        parentView = inflater.inflate(R.layout.exercise_mate, container, false);
        btn_schedule= (Button) parentView.findViewById(R.id.btn_scheduleRunning);
        btn_start= (Button) parentView.findViewById(R.id.btn_startRunning);
        btn_history= (Button) parentView.findViewById(R.id.btn_history);
        btn_dailyHistory= (Button) parentView.findViewById(R.id.btn_daily);
        btn_chart= (Button) parentView.findViewById(R.id.btn_chart);
        btn_swim=(Button)parentView.findViewById(R.id.btn_swim);
        btn_schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                TimeSchedule_Activity myFrag = new TimeSchedule_Activity();
                fragTransaction.replace(R.id.main_fragment, myFrag , "scheduleTracker");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                RunningTracker myFrag = new RunningTracker();
                fragTransaction.replace(R.id.main_fragment, myFrag , "runningTracker");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        btn_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                HistoryActivity myFrag = new HistoryActivity();
                myFrag.setSimple("onlyshow","l1");
                fragTransaction.replace(R.id.main_fragment, myFrag , "historyTracker");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });
        btn_dailyHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                HistoryActivity myFrag = new HistoryActivity();
                myFrag.setSimple("onlyshow","l2");
                fragTransaction.replace(R.id.main_fragment, myFrag , "historyTracker");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();
            }
        });

        btn_chart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                barchart myFrag = new barchart();
                fragTransaction.replace(R.id.main_fragment, myFrag , "chart");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();



            }
        });
        btn_swim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fragMan = getFragmentManager();
                FragmentTransaction fragTransaction = fragMan.beginTransaction();
                swimming myFrag = new swimming();
                fragTransaction.replace(R.id.main_fragment, myFrag , "swimming");
                fragTransaction.addToBackStack(null);
                fragTransaction.commit();



            }
        });
        return parentView;
    }

}

package com.example.awais.acclometer.CoreCode;

import android.app.TimePickerDialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TimePicker;

import com.example.awais.acclometer.R;

import java.util.Calendar;

public class TimeSchedule_Activity extends Fragment {
 public int selectedHours;
 public int selectedMins;
    Button button;
    public static boolean resetButtonText=false;
    SharedPreferences sharedpreferences;
    View v;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.activity_time_schedule,container,false);;
        button=(Button)v.findViewById(R.id.timeButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime(v);
            }
        });
        sharedpreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        // selectedHours=sharedpreferences.getInt("hours",-1);
        selectedMins=sharedpreferences.getInt("minutes",-1);

        button.setText(sharedpreferences.getString("time","Set Time"));

        if(resetButtonText)
            button.setText("Set Time");
        return v;
    }


    public void setTime(View view) {

       final Button b= (Button)view;
        Calendar mcurrentTime = Calendar.getInstance();
        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog( getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {


                resetButtonText=false;
                String a="";
                String time="";
                if(selectedHour>=0&&selectedHour<=9)
                    a="0";
                if(selectedHour>=12) {
                    time = a + selectedHour + ":" + selectedMinute + " PM";
                    b.setText(time);
                }
                else{
                    time = a + selectedHour + ":" + selectedMinute + " AM";
                    b.setText(time);
                }
                SharedPreferences.Editor editor = sharedpreferences.edit();
               // editor.putInt("hours", selectedHour);
                //editor.putInt("minutes",selectedMinute);
                editor.putString("time",time);
                editor.apply();
                editor.commit();

                Intent intent=new Intent(getContext().getApplicationContext(),TimeService.class);
                intent.putExtra("hours",selectedHour);
                intent.putExtra("minutes",selectedMinute);
                intent.putExtra("fromSC",true);
                getActivity().startService(intent);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time For Running");
        mTimePicker.show();


    }



}
